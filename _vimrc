"2021-12-24 20:41
"Written by rdj2slime
set number numberwidth=5

inoremap ' ''<Esc>i
inoremap " ""<Esc>i
inoremap { {}<Esc>i
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap ;; <Esc>

nnoremap ) ^i
nnoremap . gt
nnoremap qq bcw
nnoremap ; <Esc>
nnoremap <Space> A
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l
nnoremap <c-w> <c-w>w
nnoremap ( f(a

